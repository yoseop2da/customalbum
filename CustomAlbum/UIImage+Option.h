//
//  UIImage+Option.h
//  Album Camera
//
//  Created by 박요섭 on 2014. 7. 31..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Option)

- (UIImage *)crop:(CGRect)rect;
- (UIImage*)scaleToSize:(CGSize)size;
- (UIImage *)rotatedWithDegree:(CGFloat)degrees;
@end
