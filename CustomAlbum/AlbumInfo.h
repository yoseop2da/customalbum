//
//  AlbumInfo.h
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlbumInfo : NSObject

@property (strong) NSArray *albumNames;

+ (AlbumInfo *)sharedClient;

@end
