//
//  MainListViewController.m
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "MainListViewController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "AlbumInfo.h"
#import "MainViewCell.h"
#import "NSData+Additional.h"
#import "AddAlbumViewController.h"
#import "UIImage+Option.h"
#import "NSArray+Sort.h"

#import <iAd/iAd.h>

@interface MainListViewController ()<UIImagePickerControllerDelegate>
{
    NSInteger _selectedIndex;
}
@property (strong) ALAssetsLibrary *library;
@property (strong) UIImage *lastImage;

@end

@implementation MainListViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"AlbumCamera", @"앨범 카메라");
    self.library = [ALAssetsLibrary new];
    
    _selectedIndex = -1;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(displayAddAlbumController)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"CHANGED_ALBUM" object:nil];

    [self loadAlbums];
    
    [self registerRefreshControl];
    
    [self addBanner];
}

// iad 광고
- (void)addBanner
{
    self.canDisplayBannerAds = YES;
}

- (void)registerRefreshControl
{
    UIRefreshControl *refresh = [UIRefreshControl new];
    [refresh addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
}

- (void)loadAlbums
{
    NSMutableArray *albumArray = [NSMutableArray array];
    
    [self.library enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group != nil) {
            NSString *albumName = [group valueForProperty:ALAssetsGroupPropertyName];
            [albumArray addObject:albumName];
            
            NSArray *duplicatedCheckedArray = [[NSSet setWithArray:albumArray] allObjects];
            [AlbumInfo sharedClient].albumNames = [duplicatedCheckedArray sortArrayByOrderedAscending];
    
            if (stop) {
                [self.tableView reloadData];
            }
        }else{
            if ([AlbumInfo sharedClient].albumNames.count == 0) {
                [self displayAddAlbumController];
            }
        }
    } failureBlock:^(NSError *error) {
        ;
    }];
}

- (void)displayAddAlbumController
{
    AddAlbumViewController *optionController = [AddAlbumViewController new];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:optionController];
    navController.navigationBar.alpha = 0.0;
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.library = nil;
}

- (void)refresh
{
    [self loadAlbums];
    
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [AlbumInfo sharedClient].albumNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainViewCell *cell = [self tableView:tableView customCellForRowAtIndexPath:indexPath];
    return cell;
}

- (MainViewCell *)tableView:(UITableView *)tableView customCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMainViewCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!cell) {
        cell = [MainViewCell new];
    }
    
    cell.albumNameLabel.text = [AlbumInfo sharedClient].albumNames[indexPath.row];

    UIImage *kImage;
    NSString *cachedName = [NSString stringWithFormat:@"%@",[AlbumInfo sharedClient].albumNames[indexPath.row]];
    
    NSData *cachedData = [NSData cachedDataWithName:cachedName];

    if (cachedData) {
        UIImage *rotatedImage = [[UIImage imageWithData:cachedData] rotatedWithDegree:90];
        CGRect cropRect = CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame));
        kImage = [rotatedImage crop:cropRect];
    }
    else{
        CGRect cropRect = CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame));
        NSString *imageName = [NSString stringWithFormat:@"defaultFolderImage%d",indexPath.row%8];
        kImage = [UIImage imageNamed:imageName];
        kImage = [kImage crop:cropRect];
    }
    
    cell.albumImageView.image = kImage;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainViewCell *cell = [self tableView:tableView customCellForRowAtIndexPath:indexPath];
    return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndex = indexPath.row;
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    UIImagePickerController *imagePickerController = [UIImagePickerController new];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = (id)self;

    [self presentViewController:imagePickerController animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self cacheImage:image];
        [self.library saveImage:image toAlbum:[AlbumInfo sharedClient].albumNames[_selectedIndex] withCompletionBlock:^(NSError *error) {
            /*
             여기를 개선해야함, 
             reloadData를 해줘야 하긴 하는데,,,,,
             하면 버벅임이 있고, 안하면 실시간 업데이트가 안되고.....
             */
//            [self.tableView reloadData];
            
            if (error!=nil) {
                NSLog(@"Big error: %@", [error description]);
            }
        }];
    });
    
    [picker dismissModalViewControllerAnimated:YES];
}

// 이미지 캐싱
- (void)cacheImage:(UIImage *)image
{
#define kImageSize 320
    UIImage *newImage = [image scaleToSize:CGSizeMake(kImageSize, kImageSize)];
    NSData *data = UIImagePNGRepresentation(newImage);
    NSString *cachedName = [NSString stringWithFormat:@"%@",[AlbumInfo sharedClient].albumNames[_selectedIndex]];
    
    [data cacheWithName:cachedName];
}

- (void)resetCachedImage
{
    //케시된것을 초기화하는 방법을 간구해보자.
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSFileManager *manager = [NSFileManager defaultManager];
    
    NSError *error;
    [manager removeItemAtPath:cachePath error:&error];
}
@end
