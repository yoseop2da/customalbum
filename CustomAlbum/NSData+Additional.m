//
//  NSData+Additional.m
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

@implementation NSData (Additional)

+ (NSData *)cachedDataWithName:(NSString *)cachedName
{
    NSString *fileName = [NSString stringWithFormat:@"Album_%@",cachedName];
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [cachePath stringByAppendingPathComponent:fileName];

    return [NSData dataWithContentsOfFile:filePath];
}

- (void)cacheWithName:(NSString *)cachedName
{
    [self removeCachedImageWithName:cachedName];
    
    NSString *fileName = [NSString stringWithFormat:@"Album_%@",cachedName];
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [cachePath stringByAppendingPathComponent:fileName];
    
    BOOL writeToFile = [self writeToFile:filePath atomically:YES];
    if (!writeToFile) {
        NSLog(@"Failed to save profile image");
    }
}

- (void)removeCachedImageWithName:(NSString *)cachedName
{
    NSString *fileName = [NSString stringWithFormat:@"Album_%@",cachedName];
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [cachePath stringByAppendingPathComponent:fileName];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error;
    [manager removeItemAtPath:filePath error:&error];
}

@end
