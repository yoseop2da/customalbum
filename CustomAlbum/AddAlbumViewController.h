//
//  OptionViewController.h
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumInfo.h"

@interface AddAlbumViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *arrayField;
@property (strong) AlbumInfo *albumInfo;
@end
