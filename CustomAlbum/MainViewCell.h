//
//  MainViewCell.h
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kMainViewCell = @"MainViewCellIdentifier";

@interface MainViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;


- (id)init;
@end
