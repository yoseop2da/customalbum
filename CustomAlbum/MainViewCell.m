//
//  MainViewCell.m
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "MainViewCell.h"

@implementation MainViewCell

- (id)init
{
    NSString *nibName = nil;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        nibName = NSStringFromClass([self class]);
    }else{
        nibName = [NSString stringWithFormat:@"%@~iPad", NSStringFromClass([self class])];
    }
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    self = [nib objectAtIndex:0];
    
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
