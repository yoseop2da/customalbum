//
//  OptionViewController.m
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "AddAlbumViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "NSData+Additional.h"


@interface AddAlbumViewController ()

@property (strong) ALAssetsLibrary *assetLibrary;
@end

@implementation AddAlbumViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Add", @"추가");
    self.assetLibrary = [ALAssetsLibrary new];
    
    self.navigationController.view.backgroundColor = [UIColor redColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    UIBarButtonItem *right1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
    
    self.navigationItem.rightBarButtonItems =@[right1];
    
    self.view.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.arrayField isFirstResponder] && [touch view] != self.arrayField) {
        [self.arrayField resignFirstResponder];
    }
}

- (void)save
{
    NSString *arrayString = [self.arrayField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *albums = [self blankNameCheck:[arrayString componentsSeparatedByString:@","]];

    [self createAlbumWithCommaNames:albums];

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)createAlbumWithCommaNames:(NSArray *)albums
{
    for (id separatedByCommaName in albums) {
        NSString *name = separatedByCommaName;
        [self.assetLibrary addAssetsGroupAlbumWithName:name resultBlock:^(ALAssetsGroup *group) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANGED_ALBUM" object:nil userInfo:nil];
        } failureBlock:^(NSError *error) {
            
        }];
    }
}

- (void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSArray *)blankNameCheck:(NSArray *)array
{
    NSMutableArray *results = [NSMutableArray array];
    for (NSString *folderName in array) {
        if (folderName.length > 0) {
            [results addObject:folderName];
        }
    }
    return results;
}

@end
