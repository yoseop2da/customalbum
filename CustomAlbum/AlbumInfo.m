//
//  AlbumInfo.m
//  CustomAlbum
//
//  Created by ParkYoseop on 2014. 6. 12..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "AlbumInfo.h"

@implementation AlbumInfo

static AlbumInfo *_instance = nil;

+ (AlbumInfo *)sharedClient
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [self new];
    });
    
    return _instance;
}

#pragma mark - Initialize
- (instancetype)init
{
    self = [super init];
    if (self) {
 
    }
    return self;
}
@end
