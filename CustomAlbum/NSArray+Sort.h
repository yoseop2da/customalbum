//
//  NSArray+Sort.h
//  Album Camera
//
//  Created by 박요섭 on 2014. 8. 5..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Sort)

- (NSArray *)sortArrayByOrderedAscending;

@end
