//
//  NSArray+Sort.m
//  Album Camera
//
//  Created by 박요섭 on 2014. 8. 5..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "NSArray+Sort.h"

@implementation NSArray (Sort)

- (NSArray *)sortArrayByOrderedAscending
{
//    return [self sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
//        if ([obj1 capitalizedString] < [obj2 capitalizedString]) {
//            return NSOrderedAscending;
//        }
//        else {
//            return NSOrderedDescending;
//        }

//        if ([obj1 integerValue] > [obj2 integerValue]) {
//            return (NSComparisonResult)NSOrderedDescending;
//        }
//        
//        if ([obj1 integerValue] < [obj2 integerValue]) {
//            return (NSComparisonResult)NSOrderedAscending;
//        }
//        return (NSComparisonResult)NSOrderedSame;
//    }];
    
    return [self sortedArrayUsingComparator:^(id obj1,id obj2){
        
        if (NSOrderedDescending==[obj1 compare:obj2])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if (NSOrderedAscending==[obj1 compare:obj2])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame; }
     ];
}

@end
